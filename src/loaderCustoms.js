/// <reference path="babylon.js" />

var canvas = document.getElementById("renderCanvas");

// UI
var controlPanel = document.getElementById("controlPanel");
var divFps = document.getElementById("fps");
var aboutPanel = document.getElementById("aboutPanel");
var enableDebug = document.getElementById("enableDebug");
var status = document.getElementById("status");
var fullscreen = document.getElementById("fullscreen");
var panelIsClosed = true;
var sceneChecked;

// Babylon
BABYLON.Engine.ShadersRepository = "map/";
var engine = new BABYLON.Engine(canvas, true);
engine.enableOfflineSupport = false;
var fps = engine.getFps();
var scene;

var loadCustomScene = function (demoConstructor, then) {
/*
	if (BABYLON.Engine.isSupported()) {
        var canvas = document.getElementById("renderCanvas");
        var engine = new BABYLON.Engine(canvas, true);

        BABYLON.SceneLoader.Load("railbab/", "DemoScene.babylon", engine, function (newScene) {
            // Wait for textures and shaders to be ready
            newScene.executeWhenReady(function () {
                // Attach camera to canvas inputs
                newScene.activeCamera.attachControl(canvas);
				scene = newScene;
                // Once the scene is loaded, just register a render loop to render it
                engine.runRenderLoop(function() {
                    newScene.render();
                });
            });
        }, function (progress) {
            // To do: give progress feedback to user
        });
    } */
	
    BABYLON.SceneLoader.ShowLoadingScreen = false;
	//BABYLON.SceneOptimizerOptions.HighDegradationAllowed(30);

    engine.displayLoadingUI();

    setTimeout(function () {
        scene = demoConstructor(engine);

        if (scene.activeCamera) {
            scene.activeCamera.attachControl(canvas, false);
        }

        scene.executeWhenReady(function () {
					
            canvas.style.opacity = 1;
			
            engine.hideLoadingUI();
            BABYLON.SceneLoader.ShowLoadingScreen = true;
            if (then) {
                then(scene);
            }

            for (var index = 0; index < scene.cameras.length; index++) {
                var camera = scene.cameras[index];
                var option = new Option();
                option.text = camera.name;
                option.value = camera;

                if (camera === scene.activeCamera) {
                    option.selected = true;
                }
            }
        });
    }, 15);

	
    return;
};

// Render loop
var renderFunction = function () {
	fps = engine.getFps().toFixed();
		
    // Fps
    divFps.innerHTML =  fps + " fps";
	
    // Render scene
    if (scene) {
        if (!sceneChecked) {
            var remaining = scene.getWaitingItemsCount();
            engine.loadingUIText = "Chargements des items..." + (remaining ? (remaining + " restants") : "");
        }
		
					// setResolution(scene, 3);

		//console.log(scene.activeCamera.position);
        scene.render();
		
	//	BABYLON.SceneOptimizer.OptimizeAsync(scene, optimizeCustom());
		
        // Streams
        if (scene.useDelayedTextureLoading) {
            var waiting = scene.getWaitingItemsCount();
            if (waiting > 0) {
                status.innerHTML = "Streaming items..." + waiting + " remaining";
            } else {
                status.innerHTML = "";
            }
        }
    }
};

// Launch render loop
engine.runRenderLoop(renderFunction);

// Resize
window.addEventListener("resize", function () {
    engine.resize();
});

document.getElementById("clickableTag").addEventListener("click", function () {
    if (panelIsClosed) {
        panelIsClosed = false;
        controlPanel.style.webkitTransform = "translateY(0px)";
        controlPanel.style.transform = "translateY(0px)";
    } else {
        panelIsClosed = true;
        controlPanel.style.webkitTransform = "translateY(100px)";
        controlPanel.style.transform = "translateY(100px)";
    }
});

document.getElementById("notSupported").addEventListener("click", function () {
    document.getElementById("notSupported").className = "hidden";
});

enableDebug.addEventListener("click", function () {
console.log("cc");
    if (scene) {
        if (scene.debugLayer.isVisible()) {
            scene.debugLayer.hide();
        } else {
            scene.debugLayer.show();
        }
    }
});

fullscreen.addEventListener("click", function () {
    if (engine) {
        engine.switchFullscreen(true);
    }
});

// Check support
if (!BABYLON.Engine.isSupported()) {
    document.getElementById("notSupported").className = "";
} else {
    loadCustomScene(demo.constructor, demo.onload);
};

var setResolution = function(scene, resolution) {

	var waterMaterial = scene.getMaterialByName("waterMaterial");

	waterMaterial.refractionTexture.resize(256, true);
	waterMaterial.reflectionTexture.resize(256, true);
	//scene.getEngine().setHardwareScalingLevel(resolution);
	scene.particlesEnabled = true;
	scene.postProcessesEnabled = true;
	console.log("Optimize lvl [0] : Normal");
}

var autOptimize = function(scene, fps) {

	var waterMaterial = scene.getMaterialByName("waterMaterial");
	
		if(fps <= 50 && fps > 43) {
			waterMaterial.refractionTexture.resize(256, true);
			waterMaterial.reflectionTexture.resize(256, true);
			scene.getEngine().setHardwareScalingLevel(1);
			scene.particlesEnabled = true;
			scene.postProcessesEnabled = false;
			console.log("Optimize lvl [1]");
		}
		else if(fps <= 43 && fps > 35) {
			waterMaterial.refractionTexture.resize(256, true);
			waterMaterial.reflectionTexture.resize(256, true);
			scene.getEngine().setHardwareScalingLevel(2);
			scene.particlesEnabled = false;
			scene.postProcessesEnabled = false;
			console.log("Optimize lvl [2]");
		} 
		else if (fps <= 35) {

			waterMaterial.refractionTexture.resize(256, true);
			waterMaterial.reflectionTexture.resize(256, true);
			scene.getEngine().setHardwareScalingLevel(3);
			scene.particlesEnabled = false;
			scene.postProcessesEnabled = false;
			console.log("Optimize lvl [3]");
		}
		else
		{		
			waterMaterial.refractionTexture.resize(512, true);
			waterMaterial.reflectionTexture.resize(512, true);
			scene.getEngine().setHardwareScalingLevel(1);
			scene.particlesEnabled = true;
			scene.postProcessesEnabled = true;
			console.log("Optimize lvl [0] : Normal");
		}
}

function optimizeCustom() {
	var result = new BABYLON.SceneOptimizerOptions(60, 2000);
	result.optimizations.push(new BABYLON.TextureOptimization(0, 256));
	result.optimizations.push(new BABYLON.PostProcessesOptimization(1));
	result.optimizations.push(new BABYLON.LensFlaresOptimization(1));
	result.optimizations.push(new BABYLON.ShadowsOptimization(1));
	result.optimizations.push(new BABYLON.RenderTargetsOptimization(1));
	result.optimizations.push(new BABYLON.ParticlesOptimization(1)); 
	
return result;
}