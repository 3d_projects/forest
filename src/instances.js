objectColision = [];

//document.addEventListener("contextmenu", function (e) { e.preventDefault();	});

var CreateInstancesTestScene = function (engine) {
    scene = new BABYLON.Scene(engine);
	scene.collisionsEnabled = true;
	//Gravité -0.1
    scene.gravity = new BABYLON.Vector3(0, -0.1, 0);
	scene.enablePhysics(new BABYLON.Vector3(0, -10, 0), new BABYLON.OimoJSPlugin());
	
	//Brouillard
	
//	scene.fogMode = BABYLON.Scene.FOGMODE_EXP;
//	scene.fogColor = new BABYLON.Color3(0.9, 0.9, 0.85);
  //  scene.fogDensity = 0.2; 
			 
		var light = createLight(scene);
	
		var camera = createCamera(scene);
	
		var skybox = createSkybox(scene);

		//délimitation de la map
		createBorder(scene);
/* 
	
		// Ground
		var ground = BABYLON.Mesh.CreateGroundFromHeightMap("ground", "heightmap/forest.png", 200, 200, 150, 0, 5, scene, true);
		var groundMaterial = new GROUNDMATERIAL.GroundMaterial("ground", scene, light);
		//ground.setPhysicsState({ impostor: BABYLON.PhysicsEngine.BoxImpostor, move:false});
		
		groundMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
		groundMaterial.emissiveColor = new BABYLON.Color3(0.3, 0.3, 0.3);
		// Ajout du materiel personnaliser
		ground.material = groundMaterial;
		
		ground.receiveShadows = true;
		ground.checkCollisions = true;
		
		ground.onReady = function () {
			save(ground);
		}*/
		
		var waterMaterial = createWater(scene);
		
		//reflection & refraction
		waterMaterial.addToRenderList(skybox);
		
		//chargement du terrain
		load(scene, light);
	
    return scene;
};

var createFullMap = function(scene, light, ground) {
	
	var waterMaterial = scene.getMaterialByName("waterMaterial");
	var camera = scene.activeCamera;
	
	//	waterMaterial.addToRenderList(ground);
		
	 	// Creation de l'elevation des sommets
		var elevationControl = new WORLDMONGER.ElevationControl(ground);

		// Shadows
        var shadowGenerator = new BABYLON.ShadowGenerator(3000, light);

					
		var enemi = createEnemi(scene, ground);
		

		
		var tree = createTrees(scene, ground, shadowGenerator, 70, 170);
		
		//nom / path / filename / position / rotation / scaling / shadowGenerator / callback 
		var cabane = importMesh("cabane", "import/cabane/", "cabane.babylon", {x: -5.5, y: 6.4, z:41}, {x: 0, y:-55, z:0}, {x:0.32, y:0.32, z:0.32}, shadowGenerator, function()
		{
			var porte = scene.getMeshByName("porte");
			porte.actionManager = new BABYLON.ActionManager(scene);	
			
			var ConditionOpenDoor = new BABYLON.ValueCondition(porte.actionManager, porte, "visibility", 1, BABYLON.ValueCondition.IsEqual);
			porte.actionManager.registerAction(
				new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, 
				function() {
					porte.dispose();
				} , ConditionOpenDoor)
			);
			
		});
		
	//	simplify(cabane, {x: -5.5, y: 6.4, z:41}, {x: -5.5, y: 6.4, z:41} );

        terrainGenListener(scene, elevationControl, waterMaterial);

}

var importMesh = function (name, path, file, position, rotation, scaling, shadowGenerator, callback)	{

	BABYLON.SceneLoader.ImportMesh(name, path, file, scene, function (newMeshes, particleSystems, skeletons)
	{
		newMeshes[0].rotationQuaternion = null; // permet d'utiliser rotation (vecteur 3) a la place de rotationQuaternion (vecteur 4)
		newMeshes[0].position = new BABYLON.Vector3(parseFloat(position.x), parseFloat(position.y), parseFloat(position.z));
		newMeshes[0].rotation = new BABYLON.Vector3(parseFloat(rotation.x), parseFloat(rotation.y), parseFloat(rotation.z));
		newMeshes[0].scaling = new BABYLON.Vector3(parseFloat(scaling.x), parseFloat(scaling.y), parseFloat(scaling.z));		
		
		for(var i = 0; i < newMeshes.length; i++) {
			newMeshes[i].checkCollisions = true;
			objectColision.push(newMeshes[i]);
		//	newMeshes[i].position.y += position.y;
		}
		
		newMeshes[0].receiveShadows = false;
		shadowGenerator.getShadowMap().renderList.push(newMeshes[0]);
		
		callback();
	});	
	
};

var createTrees = function(scene, ground, shadowGenerator, range, countTree)	{

	var trees = [];
	
	// Trees
	BABYLON.SceneLoader.ImportMesh("", "import/arbre/", "tree.babylon", scene, function (newMeshes) {

		newMeshes[0].material.opacityTexture = null;
		newMeshes[0].material.backFaceCulling = false;
		newMeshes[0].isVisible = false;
		newMeshes[0].position.y = ground.getHeightAtCoordinates(0, 0); // Hauteur du terrain à la position donnée
		//newMeshes[0].checkCollisions = true;
			
		var distance = [ 0   , 5   , 15  , 25 ];
		var quality =  [ 0.8 , 0.4 , 0.3 , 0  ];
		newMeshes[0] = simplify(newMeshes[0], distance, quality, function() {
		
			shadowGenerator.getShadowMap().renderList.push(newMeshes[0]);

			for (var index = 0; index < countTree; index++) {
				var newInstance = newMeshes[0].createInstance("tree" + index);
				var x = range / 2 - Math.random() * range;
				var z = range / 2 - Math.random() * range;

				var y = ground.getHeightAtCoordinates(x, z);

				newInstance.position = new BABYLON.Vector3(x, y, z);

				newInstance.rotate(BABYLON.Axis.Y, Math.random() * Math.PI * 2, BABYLON.Space.WORLD);

				var scale = 0.5 + Math.random() * 2;
				newInstance.scaling.addInPlace(new BABYLON.Vector3(scale, scale, scale));
				
				trees.push(newInstance);

				shadowGenerator.getShadowMap().renderList.push(newInstance);
			}

			shadowGenerator.getShadowMap().refreshRate = 0;
			shadowGenerator.usePoissonSampling = true;
			
			scene.createOrUpdateSelectionOctree();
		});
		
		//trees.push(newMeshes[0]);
	 

				
		//return mergeMeshes("trees", trees, scene);
	});
}


var createEnemi = function(scene, ground) {
	var camera = scene.activeCamera;
	
	// Dude
    BABYLON.SceneLoader.ImportMesh("him", "import/homme/", "Dude.babylon", scene, function (newMeshes2, particleSystems2, skeletons2) {
        dude = newMeshes2[0];

		dude.scaling = new BABYLON.Vector3(0.04,0.03,0.04);
        dude.rotation.y = Math.PI;
        dude.position = new BABYLON.Vector3(5, ground.getHeightAtCoordinates(5,5), 5);
		dude.checkCollisions = true;

		//dude.setPhysicsState({ impostor: BABYLON.PhysicsEngine.BoxImpostor, move:true});
		
        scene.beginAnimation(skeletons2[0], 0, 100, true, 1.0);

		//Bot recherche
		scene.registerBeforeRender(function () {
			bot(dude,camera,ground);
		}); 
		
		return dude;
    });
}
	

var bot = function(bot,camera,ground)
{
	var botX = bot.position.x;
	var botZ = bot.position.z;
	var camX = camera.position.x;
	var camZ = camera.position.z;
	var diffX = botX - camX;
	var diffZ = botZ - camZ;
		
	for(var cd=0; cd < objectColision.length; cd++)	{
		if (bot.intersectsMesh(objectColision[cd], true)) {
			bot.dispose();
		}
	}
	
	if(diffX < -0.1 || diffX > 0.1)
	{
		if(camX >= botX)
		{
			bot.position.x += 0.05;
			bot.position.y = ground.getHeightAtCoordinates(bot.position.x,bot.position.z);
			bot.rotation.y = -Math.PI/2;
		}
		else
		{
			bot.position.x -= 0.05;
			bot.position.y = ground.getHeightAtCoordinates(bot.position.x,bot.position.z);
			bot.rotation.y = Math.PI/2;
		}
	}
	else
	{
		if(diffZ != 0)
		{
			if(camZ >= botZ)
			{
				bot.position.z += 0.05;
				bot.position.y = ground.getHeightAtCoordinates(bot.position.x,bot.position.z);
				bot.rotation.y = Math.PI;
			}
			else
			{
				bot.position.z -= 0.05;
				bot.position.y = ground.getHeightAtCoordinates(bot.position.x,bot.position.z);
				bot.rotation.y = -2*Math.PI;
			}
		}
	}
}

	var save = function(ground) {
		var buffer = new WORLDMONGER.Ground(ground);
		buffer.Save();    
		var vb = buffer.VerticesPosition;
		var nm = buffer.VerticesNormal;
		
		$.ajax({ url: "build/php/save_terrain.php", type:'post', data:"vb=" + vb + "&nm=" + nm });

	}
	
	var load = function(scene, light) {
	$.ajax({url:'build/php/load_terrain.php', type:'post', success:function(data){ 
		data = data.split(";"); 
		var savedPosition = data[0]; 
		var savedNormal = data[1]; 
				
		console.log("terrain chargé");
		
		loadTerrain(scene, light, savedPosition, savedNormal, createFullMap);
	}});
	
	}
	
	var loadTerrain = function(scene, light, savedPosition, savedNormal, createFullMap)
	{
		var ground = BABYLON.Mesh.CreateGround("ground", 200, 200, 150, scene, true);
		//ground.setPhysicsState({ impostor: BABYLON.PhysicsEngine.BoxImpostor, move:false});		
		ground.receiveShadows = true;
		ground.checkCollisions = true;
		ground.useOctreeForCollisions = true;
		
		var groundMaterial = new GROUNDMATERIAL.GroundMaterial("groundMaterial", scene, light);
		groundMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
		groundMaterial.emissiveColor = new BABYLON.Color3(0.3, 0.3, 0.3);
	
		ground.material = groundMaterial;
		
		console.log(savedPosition);
		
		var buffer = new WORLDMONGER.Ground(ground, savedPosition, savedNormal);
		buffer.Load();
		
		scene.createOrUpdateSelectionOctree();
		
		ground.optimize(100);
		createFullMap(scene, light, ground);
	}

	var createSkybox = function(scene)
	{
	    // Skybox
		var skybox = BABYLON.Mesh.CreateBox("skyBox", 150.0, scene);
		var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
		skyboxMaterial.backFaceCulling = false;
		skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("skybox/skybox", scene);
		skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
		skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
		skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
		skyboxMaterial.disableLighting = true;
		skybox.material = skyboxMaterial;
		skybox.infiniteDistance = true;
		
		return skybox;
	}
	
	var createLight = function(scene)
	{
	    var light = new BABYLON.DirectionalLight("dir01", new BABYLON.Vector3(0, -1, -0.3), scene);
		var light0 = new BABYLON.PointLight("Omni0", new BABYLON.Vector3(20, 10, -28.26), scene);
		// Creating light sphere
		var lightSphere0 = BABYLON.Mesh.CreateSphere("Sphere0", 16, 0.5, scene);
		
		lightSphere0.material = new BABYLON.StandardMaterial("white", scene);
		lightSphere0.material.diffuseColor = new BABYLON.Color3(0, 0, 0);
		lightSphere0.material.specularColor = new BABYLON.Color3(0, 0, 0);
		lightSphere0.material.emissiveColor = new BABYLON.Color3(1, 1, 1);

		lightSphere0.position = light0.position;
		
		var lensFlareSystem = new BABYLON.LensFlareSystem("lensFlareSystem", light0, scene);
		var flare00 = new BABYLON.LensFlare(0.2, 0, new BABYLON.Color3(1, 1, 1), "texture/soleil/lens5.png", lensFlareSystem);
		var flare01 = new BABYLON.LensFlare(0.5, 0.2, new BABYLON.Color3(0.5, 0.5, 1), "texture/soleil/lens4.png", lensFlareSystem);
		var flare02 = new BABYLON.LensFlare(0.2, 1.0, new BABYLON.Color3(1, 1, 1), "texture/soleil/lens4.png", lensFlareSystem);
		var flare03 = new BABYLON.LensFlare(0.4, 0.4, new BABYLON.Color3(1, 0.5, 1), "texture/soleil/Flare.png", lensFlareSystem);
		var flare04 = new BABYLON.LensFlare(0.1, 0.6, new BABYLON.Color3(1, 1, 1), "texture/soleil/lens5.png", lensFlareSystem);
		var flare05 = new BABYLON.LensFlare(0.3, 0.8, new BABYLON.Color3(1, 1, 1), "texture/soleil/lens4.png", lensFlareSystem);
	   
	   light.position = new BABYLON.Vector3(20, 60, 30);

	   return light;
	}
	
	var createCamera = function(scene) {
		var camera = new BABYLON.FreeCamera("Camera", new BABYLON.Vector3(0, 4, -20), scene);
		camera.speed = 0.5; 
		
		camera.angularSensibility = 1000;
		camera.keysUp = [90]; // Touche Z
		camera.keysDown = [83]; // Touche S
		camera.keysLeft = [81]; // Touche Q
		camera.keysRight = [68]; // Touche D;
		
		
		// Collisions
		camera.checkCollisions = true;
		camera.applyGravity = true;
		
		//camera.ellipsoid = new BABYLON.Vector3(0.1, 1, 0.1);
		
		return camera;
	}
	
	
	var createBorder = function(scene)	{
		//délimitation de la map
		var border0 = BABYLON.Mesh.CreateBox("border0", 1, scene);
		border0.scaling = new BABYLON.Vector3(1, 200, 200);
		border0.position.x = -50.0;
		border0.checkCollisions = true;
		border0.isVisible = false;

		var border1 = BABYLON.Mesh.CreateBox("border1", 1, scene);
		border1.scaling = new BABYLON.Vector3(1, 200, 200);
		border1.position.x = 50.0;
		border1.checkCollisions = true;
		border1.isVisible = false;

		var border2 = BABYLON.Mesh.CreateBox("border2", 1, scene);
		border2.scaling = new BABYLON.Vector3(200, 200, 1);
		border2.position.z = 50.0;
		border2.checkCollisions = true;
		border2.isVisible = false;

		var border3 = BABYLON.Mesh.CreateBox("border3", 1, scene);
		border3.scaling = new BABYLON.Vector3(200, 200, 1);
		border3.position.z = -50.0;
		border3.checkCollisions = true;
		border3.isVisible = false;
		
	}
	
	var createWater = function(scene) {

		var waterMaterial = new BABYLON.WaterMaterial("waterMaterial", scene, new BABYLON.Vector2(256, 256));
		waterMaterial.bumpTexture = new BABYLON.Texture("texture/water/waterBump.png", scene);
		waterMaterial.windForce = -4;
		waterMaterial.waveHeight = 0.02;
		waterMaterial.bumpHeight = 0.1;
		waterMaterial.waveLength = 0.1;
		waterMaterial.waveSpeed = 25.0;
		waterMaterial.colorBlendFactor = 0;
		waterMaterial.windDirection = new BABYLON.Vector2(1, 1);
		waterMaterial.colorBlendFactor = 0;

		// Water mesh
		var waterMesh = BABYLON.Mesh.CreateGround("waterMesh", 256, 256, 1, scene, false);
		waterMesh.position.y = 0.01;
		waterMesh.material = waterMaterial;
		
		return waterMaterial;
	}
	
	var mergeMeshes = function (meshName, arrayObj, scene) {
    var arrayPos = [];
    var arrayNormal = [];
    var arrayUv = [];
    var arrayUv2 = [];
    var arrayColor = [];
    var arrayMatricesIndices = [];
    var arrayMatricesWeights = [];
    var arrayIndice = [];
    var savedPosition = [];
    var savedNormal = [];
    var newMesh = new BABYLON.Mesh(meshName, scene);
    var UVKind = true;
    var UV2Kind = true;
    var ColorKind = true;
    var MatricesIndicesKind = true;
    var MatricesWeightsKind = true;

    for (var i = 0; i != arrayObj.length ; i++) {
        if (!arrayObj[i].isVerticesDataPresent([BABYLON.VertexBuffer.UVKind]))
            UVKind = false;
        if (!arrayObj[i].isVerticesDataPresent([BABYLON.VertexBuffer.UV2Kind]))
            UV2Kind = false;
        if (!arrayObj[i].isVerticesDataPresent([BABYLON.VertexBuffer.ColorKind]))
            ColorKind = false;
        if (!arrayObj[i].isVerticesDataPresent([BABYLON.VertexBuffer.MatricesIndicesKind]))
            MatricesIndicesKind = false;
        if (!arrayObj[i].isVerticesDataPresent([BABYLON.VertexBuffer.MatricesWeightsKind]))
            MatricesWeightsKind = false;
    }

    for (i = 0; i != arrayObj.length ; i++) {
        var ite = 0;
        var iter = 0;
        arrayPos[i] = arrayObj[i].getVerticesData(BABYLON.VertexBuffer.PositionKind);
        arrayNormal[i] = arrayObj[i].getVerticesData(BABYLON.VertexBuffer.NormalKind);
        if (UVKind)
            arrayUv = arrayUv.concat(arrayObj[i].getVerticesData(BABYLON.VertexBuffer.UVKind));
        if (UV2Kind)
            arrayUv2 = arrayUv2.concat(arrayObj[i].getVerticesData(BABYLON.VertexBuffer.UV2Kind));
        if (ColorKind)
            arrayColor = arrayColor.concat(arrayObj[i].getVerticesData(BABYLON.VertexBuffer.ColorKind));
        if (MatricesIndicesKind)
            arrayMatricesIndices = arrayMatricesIndices.concat(arrayObj[i].getVerticesData(BABYLON.VertexBuffer.MatricesIndicesKind));
        if (MatricesWeightsKind)
            arrayMatricesWeights = arrayMatricesWeights.concat(arrayObj[i].getVerticesData(BABYLON.VertexBuffer.MatricesWeightsKind));

        var maxValue = savedPosition.length / 3;

        arrayObj[i].computeWorldMatrix(true);
        var worldMatrix = arrayObj[i].getWorldMatrix();

        for (var ite = 0 ; ite != arrayPos[i].length; ite += 3) {
            var vertex = new BABYLON.Vector3.TransformCoordinates(new BABYLON.Vector3(arrayPos[i][ite], arrayPos[i][ite + 1], arrayPos[i][ite + 2]), worldMatrix);
            savedPosition.push(vertex.x);
            savedPosition.push(vertex.y);
            savedPosition.push(vertex.z);
        }

        for (var iter = 0 ; iter != arrayNormal[i].length; iter += 3) {
            var vertex = new BABYLON.Vector3.TransformNormal(new BABYLON.Vector3(arrayNormal[i][iter], arrayNormal[i][iter + 1], arrayNormal[i][iter + 2]), worldMatrix);
            savedNormal.push(vertex.x);
            savedNormal.push(vertex.y);
            savedNormal.push(vertex.z);
        }

        var tmp = arrayObj[i].getIndices();
        for (it = 0 ; it != tmp.length; it++) {
            arrayIndice.push(tmp[it] + maxValue);
        }
        arrayIndice = arrayIndice.concat(tmp);

        arrayObj[i].dispose(false);
    }

    newMesh.setVerticesData(BABYLON.VertexBuffer.PositionKind, savedPosition, false);
    newMesh.setVerticesData(BABYLON.VertexBuffer.NormalKind, savedNormal, false);
    if (arrayUv.length > 0)
        newMesh.setVerticesData(BABYLON.VertexBuffer.UVKind, arrayUv, false);
    if (arrayUv2.length > 0)
        newMesh.setVerticesData(BABYLON.VertexBuffer.UV2Kind, arrayUv, false);
    if (arrayColor.length > 0)
        newMesh.setVerticesData(BABYLON.VertexBuffer.ColorKind, arrayUv, false);
    if (arrayMatricesIndices.length > 0)
        newMesh.setVerticesData(BABYLON.VertexBuffer.MatricesIndicesKind, arrayUv, false);
    if (arrayMatricesWeights.length > 0)
        newMesh.setVerticesData(BABYLON.VertexBuffer.MatricesWeightsKind, arrayUv, false);

    newMesh.setIndices(arrayIndice);
    return newMesh;
};

var terrainGenListener = function(scene, elevationControl, waterMaterial)
{
// UI
	var camera = scene.activeCamera;
    var cameraButton = document.getElementById("cameraButton");
    var elevationButton = document.getElementById("elevationButton");
    var digButton = document.getElementById("digButton");
    var saveButton = document.getElementById("saveButton");
	var canvas = document.getElementById("renderCanvas");
	var mode = "CAMERA";

	saveButton.addEventListener("click", function () {
        save(scene.getMeshByName("ground"));
    });

cameraButton.addEventListener("pointerdown", function () {

        if (mode == "CAMERA")
            return;
        camera.attachControl(canvas);
        elevationControl.detachControl(canvas);

        mode = "CAMERA";
        cameraButton.className = "controlButton selected";
        digButton.className = "controlButton";
        elevationButton.className = "controlButton";
    });

    elevationButton.addEventListener("pointerdown", function () {

        if (mode == "ELEVATION")
            return;

        if (mode == "CAMERA") {
            camera.detachControl(canvas);
            elevationControl.attachControl(canvas);
        }

        mode = "ELEVATION";
        elevationControl.direction = 1;

        elevationButton.className = "controlButton selected";
        digButton.className = "controlButton";
        cameraButton.className = "controlButton";
    });

    digButton.addEventListener("pointerdown", function () {
	
        if (mode == "DIG")
            return;

        if (mode == "CAMERA") {
            camera.detachControl(canvas);
            elevationControl.attachControl(canvas);
        }

        mode = "DIG";
        elevationControl.direction = -1;

        digButton.className = "controlButton selected";
        elevationButton.className = "controlButton";
        cameraButton.className = "controlButton";
    });

    // Sliders
    $("#slider-vertical").slider({
        orientation: "vertical",
        range: "min",
        min: 2,
        max: 15,
        value: 5,
        slide: function (event, ui) {
            elevationControl.radius = ui.value;
        }
    });

    $("#slider-range").slider({
        orientation: "vertical",
        range: true,
        min: 0,
        max: 12,
        values: [0, 11],
        slide: function (event, ui) {
            elevationControl.heightMin = ui.values[0];
            elevationControl.heightMax = ui.values[1];
        }
    });
    
    $("#qualitySlider").slider({
        orientation: "vertical",
        range: "min",
        min: 0,
        max: 4,
        value: 3,
        slide: function (event, ui) {
            switch (ui.value) {
                case 4:
                    waterMaterial.refractionTexture.resize(512, true);
                    waterMaterial.reflectionTexture.resize(512, true);
                    scene.getEngine().setHardwareScalingLevel(1);
                    scene.particlesEnabled = true;
                    scene.postProcessesEnabled = true;
                    break;
                case 3:
                    waterMaterial.refractionTexture.resize(256, true);
                    waterMaterial.reflectionTexture.resize(256, true);
                    scene.getEngine().setHardwareScalingLevel(1);
                    scene.particlesEnabled = false;
                    scene.postProcessesEnabled = false;
                    break;
                case 2:
                    waterMaterial.refractionTexture.resize(256, true);
                    waterMaterial.reflectionTexture.resize(256, true);
                    scene.getEngine().setHardwareScalingLevel(2);
                    scene.particlesEnabled = false;
                    scene.postProcessesEnabled = false;
                    break;
                case 1:
                    waterMaterial.refractionTexture.resize(256, true);
                    waterMaterial.reflectionTexture.resize(256, true);
                    scene.getEngine().setHardwareScalingLevel(3);
                    scene.particlesEnabled = false;
                    scene.postProcessesEnabled = false;
                    break;
				case 0:
                    waterMaterial.refractionTexture.resize(256, true);
                    waterMaterial.reflectionTexture.resize(256, true);
                    scene.getEngine().setHardwareScalingLevel(4);
                    scene.particlesEnabled = false;
                    scene.postProcessesEnabled = false;
                    break;
            }
        }
	});
}

var simplify = function(mesh, distance, quality, callback) {
	
	var meshParams = [];
	
	for(var i = 0; i < distance.length ; i++)	{
		meshParams.push({distance: distance[i], quality: quality[i], optimizeMesh:true});
	}
	
	mesh.simplify(
		meshParams,
		false,
		BABYLON.SimplificationType.QUADRATIC, 
		function() {
	});
		
	callback();

	return mesh;	
}